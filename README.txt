CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This small module allows you to place the metadata panel (containing
menu settings, URL path settings, authoring information, promotions options,
etc.) under the node main form and displayed as vertical tabs.

You can configure the module and choose on which content type you want display
this metadata panel as vertical tabs under the main node form.

This is particularly useful if you use Field group module to organize the node
form as vertical tabs. And this is necessary if you use paragraphs module.
Because then you often need to have the full width of the browser available to
keep a node form usable.

REQUIREMENTS
------------
This module is useful :

  - if you use the seven theme administration or any administration theme which extends seven (as adminimal theme
    for example) for Drupal 8 or 9.
  - if you use the claro theme administration for Drupal 10. For sub-themes which extends Claro some CSS tweaks could be
    needed. Support is added for the Gin theme which extends Claro (but you can use the sidebar panel, which can be
    hidden natively with the Gin theme, to obtain similar feature provided by this module).


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.


CONFIGURATION
-------------

Enable the module and select the content type on which set the metadata panel
as vertical tabs under the node main form.


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------

Current maintainers:
 * flocondetoile - https://drupal.org/u/flocondetoile
